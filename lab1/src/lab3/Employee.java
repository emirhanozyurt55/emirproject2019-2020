package lab3;

public abstract class Employee {
	String name,surname;
	private Adress adress;
	private final int salary=3000;
	private final int bonus=1000;
	
	public Employee(String name, String surname,Adress adress) { /// buray� �a��r�yo 
		this.name = name;
		this.surname = surname;
		this.setAdress(adress);
	}   /// constructor here,,  when we re creating object like an employee

	@Override
	public String toString() {
		
		return "Employee [name=" + name + ",Salary=" + getSalary() + ", surname=" + surname +  getAdress().toString() + "]";
	}

	public Adress getAdress() {
		return adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getSalary() {
		return this.salary;
	}

	public int getBonus() {
		return this.bonus;
	}
	
	
}