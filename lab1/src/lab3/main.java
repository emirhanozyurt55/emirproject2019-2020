package lab3;
public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee manager1 = new Developer("Oguzhan", "Bektash", new Adress("Turkmenistan", "askhabat", "tabita", 12));
		
		Employee manager = new Manager("Emir", "Ozyurt", new Adress("Samsun", "Atakoy", "ilkadim", 13));
		
		Employee manager2 = new Manager("Adem", "Betimen", new Adress("Turkey", "Konya", "Gumustul", 31));
		
		Manager manager3 = new Manager("Omer", "Faruk", new Adress("Edirne", "Pazar", "Racal", 99));
		manager3.add(manager);
		manager3.add(manager2);
		manager3.add(manager1);
		
		System.out.println(manager3.toString());
		System.out.println(manager.toString());
		System.out.println(manager1.toString());
	}

}