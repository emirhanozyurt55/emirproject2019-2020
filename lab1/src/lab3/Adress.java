package lab3;

public class Adress {
	private String country,city,street;
	private int streetNumber;
	
	public Adress(String country, String city, String street, int streetNumber) {
		this.country = country;
		this.city = city;
		this.street = street;
		this.streetNumber = streetNumber;
	}

	@Override
	public String toString() {
		return "Adress [country=" + country + ", city=" + city + ", street=" + street + ", streetNumber=" + streetNumber
				+ "]";
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(int streetNumber) {
		this.streetNumber = streetNumber;
	}
	

}