package lab3;

import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {
	private List<Employee> employee = new ArrayList<Employee>();
	private int salary;

	public Manager(String name, String surname, Adress adress) {
		super(name, surname, adress);  /// this construction calling main classes construction in employe
		this.salary = getSalary() + getBonus(); 
		// TODO Auto-generated constructor stub
	}

	public void add(Employee employee) {
		this.employee.add(employee);
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(
				"Employee [name=" + name + ",Salary=" + salary + ", surname=" + surname + getAdress().toString() + "]");

		for (Employee employee : employee) { 
			str.append(employee.toString()); /// loop trough employee array and pr�nt �nfo tostring kodu print etmek i�in.

		}
		return str.toString();

	}
	
	public List<Employee> getEmployee(){
		return employee ;
		}
		public int getSalary() {
			return salary;
			
		}
	}