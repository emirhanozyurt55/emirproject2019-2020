package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import lab3.Adress;
import lab3.Employee;
import lab3.Manager;

class AdressClassTest {

	@Test
	//Testing if Objects variables don't matches with String  variable 
	void EmployeeAddressVariablesTest() {
		System.out.println("### Manager Address Variable Test ###");
		Employee manage3 = new Manager("Emirhan", "Ozyurt", new Adress("Turkey", "Samsun", "ilkad�m", 11));
		
		String Country="USA"; String City="Carrot"; String Street="Rose"; int StreetNumber=95;
		
		System.out.println("Manager Address Test-> String Country");
		assertNotEquals(Country, manage3.getAdress().getCountry());
		System.out.println("Manager Address Test-> String City");
		assertNotEquals(City, manage3.getAdress().getCity());
		System.out.println("Manager Address Test-> String Street");
		assertNotEquals(Street, manage3.getAdress().getStreet());
		System.out.println("Manager Address Test-> int StreetNumber");
		assertNotEquals(StreetNumber, manage3.getAdress().getStreetNumber());
	}
	
	@Test
	// Testing if Adress variables  matches with String  variable 
	void AdressVariablesTest() {
		System.out.println("###  Address Variable Test ###");
		Adress manage3 =  new Adress("Turkey", "Samsun", "ilkad�m", 11);
		
		String Country="Turkey"; String City="Samsun"; String Street="ilkad�m"; int StreetNumber=11;
		
		System.out.println("Manager Address Test-> String Country");
		assertEquals(Country, manage3.getCountry());
		System.out.println("Manager Address Test-> String City");
		assertEquals(City, manage3.getCity());
		System.out.println("Manager Address Test-> String Street");
		assertEquals(Street, manage3.getStreet());
		System.out.println("Manager Address Test-> int StreetNumber");
		assertEquals(StreetNumber, manage3.getStreetNumber());
	}
	@Test
	// Testing if 2 values of 2 almost same objects equal and not equal
	void AdressObjectVariablesTest() {
		System.out.println("###  Address Object Variables Test ###");
		Adress manage3 =  new Adress("Turkey", "Samsun", "ilkad�m", 11);
		Adress manage4 =  new Adress("Istanbul", "Samsun", "EskiSehir", 11);
		assertEquals(manage3.getCity(),manage4.getCity());
		assertNotEquals(manage3.getCountry(),manage4.getCountry());
	}
	@Test
	// Testing Adress Classes ToString function if its  equal to String variable 
	void EmployeeAdressToStringTest() {
		System.out.println("### Manager Address Test--> ToString ###");
		Adress manage3 = new Adress("Turkey", "Samsun", "ilkad�m", 11);
		String adress = "Adress [country=" + manage3.getCountry() + ", city=" + manage3.getCity() + ", street=" + manage3.getStreet() + ", streetNumber=" + manage3.getStreetNumber()+ "]";
		assertEquals(adress,manage3.toString());
	}
	
}