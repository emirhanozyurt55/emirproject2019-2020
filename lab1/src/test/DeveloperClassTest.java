package test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import lab3.Adress;
import lab3.Developer;
import lab3.Employee;
import lab3.Manager;

class DeveloperClassTest {

	@Test
	// Testing if String Name matches with objects Name variable
	void EmployeeNameTest() {
		System.out.println("Manager Name Test");
		Employee manager = new Developer("Emirhan", "�zyurt", new Adress("Turkey", "Samsun", "ilkad�m", 11));
		String name = "Emirhan";
		assertEquals(name, manager.getName());
	}

	@Test
	// Testing if String Surname matches with objects Surname variable
	void EmployeeSurnameTest() {
		System.out.println("Manager Surname Test");
		Employee manager1 = new Developer("Emirhan", "�zyurt", new Adress("Turkey", "Samsun", "ilkad�m", 11));
		String surname = "�zyurt";
		assertEquals(surname, manager1.getSurname());
	}

	@Test
	// Testing if Objects variables matches with String variable
	void EmployeeAddressVariablesTest() {
		System.out.println("### Manager Address Test ###");
		Employee manage3 = new Developer("Emirhan", "�zyurt", new Adress("Turkey", "Samsun", "ilkad�m", 11));
		String Country = "Turkey";
		String City = "Samsun";
		String Street = "ilkad�m";
		int StreetNumber = 11;
		System.out.println("Manager Address Test-> String Country");
		assertEquals(Country, manage3.getAdress().getCountry());
		System.out.println("Manager Address Test-> String City");
		assertEquals(City, manage3.getAdress().getCity());
		System.out.println("Manager Address Test-> String Street");
		assertEquals(Street, manage3.getAdress().getStreet());
		System.out.println("Manager Address Test-> int StreetNumber");
		assertEquals(StreetNumber, manage3.getAdress().getStreetNumber());
	}

	@Test
	// Testing Manager Classes ToString function if its equal to String variable
	void EmployeeAdressToStringTest() {
		System.out.println("### Manager Address Test--> ToString ###");
		Employee manage3 = new Developer("Emirhan", "�zyurt", new Adress("Turkey", "Samsun", "ilkad�m", 11));
		String adress = "Adress [country=" + manage3.getAdress().getCountry() + ", city="
				+ manage3.getAdress().getCity() + ", street=" + manage3.getAdress().getStreet() + ", streetNumber="
				+ manage3.getAdress().getStreetNumber() + "]";
		assertNotEquals(adress, manage3.toString());
	}

	@Test
	// Testing if employee list is not equal to Objects employee list
	// ( its not equal Because one list has Developer and other Manager)
	void EmployeeArrayListTest() {
		List<Employee> employee = new ArrayList<Employee>();
		Manager manage3 = new Manager("Emirhan", "�zyurt", new Adress("Turkey", "Samsun", "ilkad�m", 11));
		Employee manager1 = new Developer("Oguzhan", "Bektash", new Adress("Turkmenistan", "Fargona", "tabita", 27));
		Employee manager2 = new Manager("Oguzhan", "Bektash", new Adress("Turkmenistan", "Fargona", "tabita", 27));

		manage3.add(manager1);
		employee.add(manager2);
		
		assertNotEquals(employee, manage3.getEmployee());
	}

}