	public class Car {

		private String brand;
		private double engineSize;
		private String model;
		private int productionDate;
		private String colour;
		/// for car specification
		
		
		public Car(String brand, String model, int productionDate, String colour, double engineSize) {
			super();
			this.brand = brand;
			this.engineSize = engineSize;
			this.model = model ;
			this.productionDate = productionDate;
			this.colour = colour;
		}
		
		public String getModel() {
			return model;
		}
		public void setModel(String model) {
			this.model = model;
		}
		public String getBrand() {
			return brand;
		}
		public void setBrand(String brand) {
			this.brand = brand;
		}
		public int getProductionDate() {
			return productionDate;
		}
		public void setProductionDate(int productionDate) {
			this.productionDate = productionDate;
		}
		public String getColour() {
			return colour;
		}
		public void setColour(String colour) {
			this.colour = colour;
		}
		public double getEngineSize() {
			return engineSize;
		}
		public void setEngineSize(double engineSize) {
			this.engineSize = engineSize;
		}
		
		
		

}
