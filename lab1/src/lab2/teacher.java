package lab2;

public class teacher extends person {
	String Title;

	public teacher(String name, String surname, int age, String Title, String email, int phoneNumber) {
		super(name, surname, age, email, phoneNumber);
		this.Title = Title;
	}

	public String DisplayName() {
		String info = "Name: " + name + " | Title: " + Title;
		return info;
	}
}