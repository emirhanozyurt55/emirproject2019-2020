package lab2;

public abstract class person {
	String email;
	int phoneNumber;
	String name;
	String surname;
	int age;

	public person(String name, String surname, int age, String email, int phoneNumber) {
		super();
		this.age = age;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public String DisplayName() {
		return name;
	}
}

///person info 